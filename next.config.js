const webpack = require('webpack');

/** @type {import('next').NextConfig} */
const nextConfig = {
  webpack: (config, _context) => {
    return config;
  }
}

module.exports = nextConfig;
