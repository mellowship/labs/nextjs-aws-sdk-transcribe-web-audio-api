"use client";

import React, { useEffect } from 'react';

import * as awsTranscribe from '@/utils/aws-transcribe.utils';

export default function AudioTranscription() {
  useEffect(() => {
    (async () => {
      const audioMediaStream: MediaStream = await navigator.mediaDevices.getUserMedia({ audio: true });
      const audioContext = new AudioContext({ sampleRate: 44100 });

      await audioContext.audioWorklet.addModule(new URL("@/worklets/audio-chopper-processor.worklet.js", import.meta.url));
      const audioStreamSource = audioContext.createMediaStreamSource(audioMediaStream);

      const audioRecoderWorklet = new AudioWorkletNode(audioContext, 'audio-chopper-processor.worklet');

      // audioStreamSource.connect(audioRecoderWorklet).connect(audioContext.destination);
      audioStreamSource.connect(audioRecoderWorklet);

      const transcripts = awsTranscribe.startStreamTranscription({
        sampleRate: audioContext.sampleRate,
        audioStream: async function* () {
          const audioReadableStream = new ReadableStream<Uint8Array>({
            start(controller) {
              audioRecoderWorklet.port.onmessage = (e) => {
                controller.enqueue(e.data);
              }
            },
          });

          const reader = audioReadableStream.getReader();

          try {
            while (true) {
              const { done, value } = await reader.read();

              if (done) {
                break;
              }

              yield { AudioEvent: { AudioChunk: value } };
            }
          } finally {
            reader.releaseLock();
          }
        }
      });

      for await (const transcript of transcripts) {
        console.log(transcript);
      }
    })();
  }, []);

  return (
    <div className='w-screen h-screen bg-slate-900 flex justify-center items-center text-4xl'>
      NextJS + React + TypeScript + AWS SDK Transcribe + Web Audio API
    </div>
  );
}
