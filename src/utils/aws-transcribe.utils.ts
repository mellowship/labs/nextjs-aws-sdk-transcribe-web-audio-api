import map from "lodash/map"

import * as awsTranscribeSdk from '@aws-sdk/client-transcribe-streaming';

const client = new awsTranscribeSdk.TranscribeStreaming({
  region: 'us-east-1',
  credentials: {
    accessKeyId: process.env.NEXT_PUBLIC_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.NEXT_PUBLIC_AWS_SECRET_ACCESS_KEY,
  }
});

export async function* startStreamTranscription({
  sampleRate,
  audioStream,
}: {
  sampleRate: number;
  audioStream: () => AsyncIterable<awsTranscribeSdk.AudioStream>;
}): AsyncGenerator<string> {
  const data = await client.startStreamTranscription({
    AudioStream: audioStream(),
    LanguageCode: awsTranscribeSdk.LanguageCode.EN_US,
    MediaEncoding: awsTranscribeSdk.MediaEncoding.PCM,
    MediaSampleRateHertz: sampleRate,
  });

  for await (const event of data.TranscriptResultStream ?? []) {
    if (event.TranscriptEvent) {
      const results = event.TranscriptEvent.Transcript?.Results ?? [];

      const transcripts = map(results, (result) => {
        return map(result.Alternatives ?? [], (alternative) => {
          return map(alternative.Items ?? [], (item) => {
            return item.Content;
          }).join('');
        }).join(' ');
      });

      for (const transcript of transcripts) {
        yield transcript;
      }
    }
  }
}
