import AudioTranscription from '@/components/audio-transcription.component'

export default function Home() {
  return (
    <main>
      <AudioTranscription />
    </main>
  )
}
