export {}

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NEXT_PUBLIC_AWS_ACCESS_KEY_ID: string
      NEXT_PUBLIC_AWS_SECRET_ACCESS_KEY: string
    }
  }
}
